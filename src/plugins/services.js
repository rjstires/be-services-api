const _ = require('lodash');
const Service = require('./services.model');

module.exports = function (options) {
    const seneca = this;
    const plugin = 'services';
    var restRoutes = require('../common/restRoutes');

    options = seneca.util.deepextend({}, options);

    seneca.add({role: plugin, cmd: 'create'}, createService);

    seneca.add({role: plugin, cmd: 'read'}, function (args, cb) {
        cb(null, {status: 'ok', location: 'read'})
    });

    seneca.add({role: plugin, cmd: 'update'}, function (args, cb) {
        cb(null, {status: 'ok', location: 'update'})
    });

    seneca.add({role: plugin, cmd: 'delete'}, function (args, cb) {
        cb(null, {status: 'ok', location: 'delete'})
    });

    seneca.add({role: plugin, cmd: 'list'}, function (args, cb) {
        cb(null, {status: 'ok', location: 'list'})
    });

    /* Use our common rest routes */
    const routes = Object.assign({}, restRoutes, {
        /* Custom routes go here */
    });

    seneca.act({role: 'web'}, {
        routes: {
            prefix: '/v1/services',
            pin: 'role:services,cmd:*', // You can not use an Object literal here. seneca-web/lib/mapper.js:45
            map: routes
        }
    }, function (err, routes) {
        console.log(err || routes);
    });

    return plugin;
};

/**
 *
 * @param args
 * @param callback
 */
function createService(args, callback) {
    Service.create(args)
        .then((results)=> {
            callback(null, results);
        })
        .catch((error)=> {
            callback(error);
        });
}