const Promise = require('bluebird');

var create = function(option){
    return new Promise((res, rej)=>{
        setTimeout(() => {
            res({status: 201, message: 'You did it!'});
        }, 2500);
    });
};

var read = function(args, cb){
    cb(null, {status: 'ok'});
};

var update = function(args, cb){
    cb(null, {status: 'ok'});
};

var destroy = function(args, cb){
    cb(null, {status: 'ok'});
};

var list = function(args, cb){
    cb(null, {status: 'ok'});
};

module.exports = {
    create: create,
    read: read,
    update: update,
    destroy: destroy,
    list: list
};