module.exports = {
    read: {name: '', GET: true, suffix: '/:id'},
    list: {name: '', GET: true},
    create: {name: '', POST: true},
    update: {name: '', POST: true, suffix: '/:id'},
    delete: {name: '', 'DELETE': true, suffix: '/:id'}
};