var Seneca = require('seneca');
var Web = require('seneca-web');
var Express = require('Express');

/**
 * Plugins
 */
var Services = require('./src/plugins/services');

var config = {
    adapter: require('seneca-web-adapter-express'),
    context: Express()
};

var seneca = Seneca()
    .use(Web, config)
    .use(Services)
    .ready(() => {
        var server = seneca.export('web/context')();

        server.listen('4000', () => {
            console.log('server started on: 4000')
        });

        console.log(seneca.list());
    });